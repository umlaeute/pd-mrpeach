#!/usr/bin/make -f
# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2015-2022 IOhannes m zmölnig <umlaeute@debian.org>
# Description: Main Debian packaging script for mrpeach


#enable hardening
export DEB_BUILD_MAINT_OPTIONS=hardening=+all

# get SOURCE_DATE_EPOCH and DEB_VERSION
include /usr/share/dpkg/pkg-info.mk

%:
	dh $@ --buildsystem=pd_lib_builder

override_dh_installchangelogs:
	dh_installchangelogs debian/upstream_changelog

override_dh_gencontrol:
	dh_gencontrol --package=pd-osc -- -v1:$(DEB_VERSION_UPSTREAM_REVISION)
	dh_gencontrol --remaining-packages
	for p in cmos mrpeach-net osc slip xbee; do\
		if [ -f debian/.debhelper/pd-$$p/dbgsym-root/DEBIAN/control ]; then\
			echo "Conflicts: pd-mrpeach-dbgsym" >> debian/.debhelper/pd-$$p/dbgsym-root/DEBIAN/control\
	; fi; done
	if [ -f debian/.debhelper/pd-mrpeach/dbgsym-root/DEBIAN/control ]; then\
		echo "Conflicts: pd-cmos-dbgsym, pd-mrpeach-net-dbgsym, pd-osc-dbgsym, pd-slip-dbgsym, pd-xbee-dbgsym" >> debian/.debhelper/pd-mrpeach/dbgsym-root/DEBIAN/control\
	;fi

DEB_COPYRIGHT_CHECK_IGNORE_REGEX = \
        debian/.*|midifile/I_Wanna_Be_Sedated\.mid
# licensecheck v1
.PHONY: licensecheck
licensecheck:
	LANG=C.UTF-8 licensecheck \
		-i "^($(DEB_COPYRIGHT_CHECK_IGNORE_REGEX))$$" \
		--check '.*' --recursive --deb-machine --lines 0 * \
		> debian/copyright_newhints
	cmp debian/copyright_hints debian/copyright_newhints \
		&& rm debian/copyright_newhints
