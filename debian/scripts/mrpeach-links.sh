#!/bin/sh

## e.g. "pd-slip"
PKGNAME=$1
## e.g. "/usr/lib/pd/extra/slip"
INDIR=$2

PKGDIR=debian/${PKGNAME}
OUTDIR=/usr/lib/pd/extra/mrpeach

find "${PKGDIR}${INDIR}" -maxdepth 1 -type f -name "*.pd*" -not -name "*-meta.pd" | while read f
do
 infile=${f#${PKGDIR}/}
 outfile=${OUTDIR}/${infile##*/}
 echo "${infile}" "${outfile}"
done
